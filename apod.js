document.addEventListener('keydown', function(event) {
  if (event.ctrlKey && event.key === 'z') {
    alert('Undo!');
  }
});


function nasarequester(){
  var parsedUrl = new URL(window.location.href);
  if(parsedUrl.searchParams.get("whdurl") != null){
    document.getElementById("whdurl").checked = true;
  }
  requri="https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY"
  if(parsedUrl.searchParams.get("dater") != null){
    requri += "&date="+parsedUrl.searchParams.get("dater");
  }
  var request = new XMLHttpRequest();

  //Envoie de la requete
  request.open('GET', requri, true);


  request.onload = function () {
    // Ici accès au JSON
    var data = JSON.parse(this.response);

    if (request.status >= 200 && request.status < 400) {
      builder(data.title, data.url, data.explanation, data.copyright, data.hdurl)
    } else {
      console.log('error');
      if(request.status>=400 || request.status <= 200){
        document.getElementById("title").innerHTML = "Biblihothèque indisponible"
        document.getElementById("desciption").innerHTML = "<table><tbody><tr><td>HTTP code :</td><td id='httpError'></td></tr><tr></tr><tr></tr><tr><td><u>Une erreure est survenue :</u></td><td>Vous tentez peut être d'atteindre une entrée trop loin dans le passé (début du calendrier le 20-06-1995).</td></tr><tr><td></td><td>Vous tentez peut être d'atteindre une date dans le futur.</td></tr><tr><td></td><td>L'entrée du jour n'est pas encore publié par la NASA.</td></tr></tbody></table>";
        document.getElementById("httpError").innerHTML = request.status;
      }
    }

  }

  // Envoie de la requete
  request.send();
}

function ajusterDate(){
  var parsedUrl = new URL(window.location.href);
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();
  if(parsedUrl.searchParams.get("dater") != null){
    uptodate=parsedUrl.searchParams.get("dater");
  }
  else{
    var uptodate=yyyy+"-"+mm+"-"+dd;
  }
  var maxDate = yyyy+"-"+mm+"-"+dd;
  var dateControl = document.querySelector('input[type="date"]');
  dateControl.value = uptodate;
  dateControl.min = "1995-06-20";
  dateControl.max = maxDate;
}

function builder(title, pictureLink,desciption,copyright,hdurl){
  var parsedUrl = new URL(window.location.href);
  document.getElementById("urlPic").style.visibility = "visible";
  document.getElementById("title").innerHTML = title
  //déterminer le type d'objet
  var substring = "youtube";
  if(pictureLink.includes(substring)==true){
    htmlToPut='<iframe width="800" height="500" src="'+pictureLink+'" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    document.getElementById("urlPic").href=pictureLink;
  }
  else{
    if(hdurl!=null && parsedUrl.searchParams.get("whdurl") != null){
      htmlToPut='<img style="max-height: 500px; max-width:80%; color: #ee6c4d;" src="'+hdurl+'" alt="File not supported, to see it click \'File Link\' button down bellow"></img>'
      document.getElementById("urlPic").href=hdurl;
    }
    else{
      htmlToPut='<img style="max-height: 500px; max-width:80%; color: #ee6c4d;" src="'+pictureLink+'" alt="File not supported, to see it click \'File Link\' button down bellow"></img>'
      document.getElementById("urlPic").href=pictureLink;
    }
  }
  document.getElementById("picture").innerHTML = htmlToPut;
  document.getElementById("desciption").innerHTML = desciption;
  document.getElementById("copyright").innerHTML = "Copyright : "+copyright;

}
