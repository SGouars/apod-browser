# Apod Browser

Based on the [NASA's APOD API](https://api.nasa.gov/), this project allows you to browse the API. By choosing a date you will be able to see the NASA's astronomic picture of that day.

*Instalation :*
 - Clone the repository.
 - Execute apod.html (no server needed, evrything is client side).
